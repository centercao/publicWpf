### 看我!看我！看我！
### 该项目原始使用的是WCF/ADO.NET, 前端使用的是WPF(.NETFramework)。

# 那么！
## 基于.NET Core的船新版本:
### 码云地址: https://gitee.com/zhgg666/WPF-Xamarin-Blazor-Examples

### Github地址: https://github.com/HenJigg/WPF-Xamarin-Blazor-Examples


本项目为开源WPF MVVM 示例,关于工具介绍如下:
* 基于:      .netFramework4.5；
* 开发工具： Visua Studio2017
* 数据库:    Mssql 2012
* 官方项目介绍: https://www.cnblogs.com/zh7791/p/9761935.html


# 关于开发WPF框架说明：
目前该项目仍然在持续的更新和不断的完善当中,支持自行修改, 后期提供完整从后端到UI得自动生成, 框架构建文档, API设计文档, 提供免费得WPF相关技术支持。

登录窗口:
![输入图片说明](https://images.gitee.com/uploads/images/2018/1228/230837_86b5dbd8_1122787.png "Login.png")

首页效果:
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/094252_db948953_1122787.png "Main.png")

数据验证:
![输入图片说明](https://images.gitee.com/uploads/images/2018/1214/171419_0f6385d2_1122787.png "User.png")

# 皮肤设置：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1228/230850_927fbeba_1122787.png "Skin.png")

# 菜单检测：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/175355_ea18cf9a_1122787.png "Menu.png")

# 角色配置：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1214/171521_55dfc3cc_1122787.png "Qx.png")

# 即时通讯(演示,开发中)：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1104/152209_a171a49c_1122787.png "Msg.png")

# 代码生成工具：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1129/180221_23a8130c_1122787.png "Gneeretor.png")

# 关于作者：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/175510_769595f6_1122787.png "About.png")

# QQ交流群 : 874752819
# 博客地址:www.cnblogs.com/zh7791
# Gitee地址: www.gitee.com/zhgg666

## 当前版本
* UI采用开源的MD组件；
* 前后端完全分离。采用WPF MVVM结构；
* 后端采用 EF+Unity


## 运行说明
* 1.目前测试服务器,可以直接运行, 默认账套只拥有浏览权限。
* 2.如需了解数据库结构, 请务必加入QQ群获取最近的数据库脚本。
步骤.进入MSSQL执行ZFS.SQL脚本 (QQ群文件), 导入MSSQL数据库, 根据当前导入的MSSQL类型, 进行本地文件修改。


